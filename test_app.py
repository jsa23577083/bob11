import unittest
import app

class TestCal(unittest.TestCase):

    def test_add(self):
        instance = app.Server_run()
        result = instance.add(2,3)
        self.assertEqual(int(result),5)

    def test_sub(self):
        instance = app.Server_run()
        result = instance.sub(4,3)
        self.assertEqual(int(result),1)

if __name__ == "__main__":
    unittest.main()

